package com.example.shemajamebeliati.Fragments


import android.content.Context
import android.graphics.drawable.Drawable
import android.util.Log.d
import android.view.View
import android.widget.Toast
import androidx.core.graphics.drawable.DrawableCompat
import androidx.core.graphics.drawable.toDrawable
import androidx.recyclerview.widget.GridLayoutManager
import com.example.shemajamebeliati.R
import com.example.shemajamebeliati.adapter.CleanCircle
import com.example.shemajamebeliati.adapter.fillCircle
import com.example.shemajamebeliati.adapter.itemsAdapter
import com.example.shemajamebeliati.databinding.FragmentSecurityBinding
import com.google.android.material.snackbar.Snackbar


class SecurityFragment : BaseFragment<FragmentSecurityBinding>(FragmentSecurityBinding::inflate) {
    var adapter = itemsAdapter()
    var whiteDrawable : Drawable=R.color.white.toDrawable()
    var purpleDrawable : Drawable=R.color.black.toDrawable()
    override fun start() {
        initAdapter()
    }

    fun initAdapter() {
        binding.rvItems.adapter = adapter
        binding.rvItems.layoutManager = GridLayoutManager(context, 3)
        adapter.changeColor = object : fillCircle {
            override fun invoke(list : MutableList<Int>) {
                when(list.size)
                {
                    1 -> binding.ivFirst.setImageResource(R.drawable.ic_fill_circle)
                    2 -> binding.ivSecond.setImageResource(R.drawable.ic_fill_circle)
                    3 -> binding.ivThird.setImageResource(R.drawable.ic_fill_circle)
                    4 ->{
                        binding.ivFourth.setImageResource(R.drawable.ic_fill_circle)
                        if(list==mutableListOf(0,9,3,4))
                            Snackbar.make(view!!, "SUCCESS", Snackbar.LENGTH_LONG).show();
                        else
                            Snackbar.make(view!!, "FAIL", Snackbar.LENGTH_LONG).show();
                    }
                    else ->
                        Snackbar.make(view!!, "www.journaldev.com", Snackbar.LENGTH_LONG).show();
                }
            }
        }
        adapter.backColor=object : CleanCircle {
            override fun invoke(size: Int) {
                d("log","tamriko")
                when(size) {

                    1 -> binding.ivFirst.setImageResource(R.drawable.ic_circles)
                    2 -> binding.ivSecond.setImageResource(R.drawable.ic_circles)
                    3 -> binding.ivThird.setImageResource(R.drawable.ic_circles)
                    4 -> binding.ivFourth.setImageResource(R.drawable.ic_circles)
                }
            }
        }
    }
}