package com.example.shemajamebeliati.adapter

import android.util.Log.d
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.shemajamebeliati.R
import com.example.shemajamebeliati.databinding.ItemBinding
import com.example.shemajamebeliati.model.item

typealias fillCircle  = (MutableList<Int>) -> Unit
typealias CleanCircle  = (Int) -> Unit
class itemsAdapter : RecyclerView.Adapter<itemsAdapter.ViewHolder>() {
    var passcode = mutableListOf<Int>()
    lateinit var changeColor : fillCircle
    lateinit var backColor : CleanCircle
    var Items= mutableListOf<item>(
        item("1", 1),
        item("2", 2),
        item("3", 3),
        item("4", 4),
        item("5", 5),
        item("6", 6),
        item("7", 7),
        item("8", 8),
        item("9", 9),
        item("0", "fingerPrint", R.drawable.ic_touch__id),
        item("0", 0),
        item("pr", "", R.drawable.ic_del_icon)
    )

    inner class ViewHolder(val binding: ItemBinding) : RecyclerView.ViewHolder(binding.root) , View.OnClickListener  {
        lateinit var item : item
        fun bind()
        {   item=Items[adapterPosition]
            binding.ivCircle.setOnClickListener(this)
            when(item.id) {
             is Int   -> binding.tvNum.text=Items[adapterPosition].name
             else -> binding.ivSymb.setImageResource(item.src!!)
            }
        }

        override fun onClick(v: View?) {
            d("tamriko",item.id.toString())
            if(item.id is Int) {
                if(passcode.size!=4)
                     passcode.add((item.id.toString().toInt()))
                changeColor(passcode)
            }
            if(item.id.toString() == ""){
                d("tamriko",passcode.size.toString())
                backColor(passcode.size)
                if(passcode.size!=0)
                     passcode.removeLast()
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(ItemBinding.inflate(LayoutInflater.from(parent.context), parent, false))


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
       holder.bind()
    }

    override fun getItemCount(): Int = 12 //shopItems.size
}
