package com.example.shemajamebeliati.Extensions


fun Any.isInt(): Boolean = this is Int
